package org.fpasti.jdbc.esqlj.elastic.query.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.fpasti.jdbc.esqlj.EsConnection;
import org.fpasti.jdbc.esqlj.elastic.model.ElasticSearchableType;
import org.fpasti.jdbc.esqlj.elastic.query.AbstractOneShotQuery;
import co.elastic.clients.elasticsearch.cat.AliasesResponse;
import co.elastic.clients.elasticsearch.cat.IndicesResponse;
import co.elastic.clients.elasticsearch.cat.aliases.AliasesRecord;
import co.elastic.clients.elasticsearch.cat.indices.IndicesRecord;
import co.elastic.clients.elasticsearch.indices.GetAliasRequest;
import co.elastic.clients.elasticsearch.indices.GetAliasResponse;
import co.elastic.clients.elasticsearch.indices.GetIndexRequest;
import co.elastic.clients.elasticsearch.indices.GetIndexResponse;

/**
* @author  Fabrizio Pasti - fabrizio.pasti@gmail.com
*/

public class IndicesQuery extends AbstractOneShotQuery {
			
	private static String[] COLUMNS =  {"TABLE_CAT", "TABLE_SCHEM", "TABLE_NAME", "TABLE_TYPE", "REMARKS", "TYPE_CAT", "TYPE_SCHEM", "TYPE_NAME", "SELF_REFERENCING_COL_NAME", "REF_GENERATION"};
	
	public IndicesQuery(EsConnection connection, ElasticSearchableType... types) throws SQLException {
		super(connection, "system_indices", COLUMNS);
		
		if(Arrays.asList(types).contains(ElasticSearchableType.INDEX)) {
			init(ElasticSearchableType.INDEX);
		}
		
		if(Arrays.asList(types).contains(ElasticSearchableType.ALIAS)) {
			init(ElasticSearchableType.ALIAS);
		}
	}

	public void init(ElasticSearchableType type) throws SQLException {
		List<IndicesInfo> indices = type == ElasticSearchableType.INDEX ? retrieveIndices() : retrieveAliases();
		indices.forEach(indice -> {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("TABLE_NAME", indice.name);
			data.put("TABLE_TYPE", type == ElasticSearchableType.INDEX ? "TABLE" : "VIEW");
			if (ElasticSearchableType.INDEX == type) {
			  data.put("REMARKS", String.format("docs.count: %s, store.size: %s", indice.count, indice.size));
			} else {
			  data.put("REMARKS", "");
			}
			insertRowWithData(data);
		});
	}
	
	private List<IndicesInfo> retrieveIndices() throws SQLException {
		try {
		  IndicesResponse resp = getConnection().getElasticClient().cat().indices();
		  List<IndicesRecord> valueBody = resp.valueBody();
		  return valueBody.stream().filter(p -> !p.index().startsWith(".")).map(item -> {
            IndicesInfo i = new IndicesInfo();
            i.name = item.index();
            i.size = item.storeSize();
            i.count = item.docsCount();
            return i;
          }).collect(Collectors.toList());
//			GetIndexRequest indexRequest = new GetIndexRequest.Builder().index("*").build();
//			GetIndexResponse indexResponse = getConnection().getElasticClient().indices().get(indexRequest);
//			List<String> indices = new ArrayList<String>(indexResponse.result().keySet());
//			return indices.stream().sorted().collect(Collectors.toList());
		} catch(IOException e) {
			throw new SQLException("Failed to retrieve indices and aliases from Elastic");
		}
	}

	private List<IndicesInfo> retrieveAliases() throws SQLException {
		try {
		  AliasesResponse resp = getConnection().getElasticClient().cat().aliases();
		  List<AliasesRecord> valueBody = resp.valueBody();
		  return valueBody.stream().map(item -> {
            IndicesInfo i = new IndicesInfo();
            i.name = item.alias();
            return i;
          }).collect(Collectors.toList());
//			GetAliasRequest indexRequest = new GetAliasRequest.Builder().index("*").build();
//			GetAliasResponse indexResponse = getConnection().getElasticClient().indices().getAlias(indexRequest);
//			List<String> aliases = new ArrayList<String>(indexResponse.result().keySet());
//			return aliases.stream().sorted().collect(Collectors.toList());
		} catch(IOException e) {
			throw new SQLException("Failed to retrieve indices and aliases from Elastic");
		}
	}
	
	public class IndicesInfo {
	  String name;
	  String size;
	  String count;
	}
}
